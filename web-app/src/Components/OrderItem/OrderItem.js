import React from 'react';

const OrderItem = ({amount, price, name}) => {
    return (
        <div className="OrderItem">
            {amount} X {name} {price * amount}
        </div>
    );
};

export default OrderItem;