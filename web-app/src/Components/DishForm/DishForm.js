import React, {useEffect, useState} from 'react';
import {Button, Container, Form} from "react-bootstrap";
import Backdrop from "../UI/Backdrop/Backdrop";
import Preloader from "../UI/Preloader/Preloader";
import {useDispatch, useSelector} from "react-redux";
import {sendDish, updateDish} from "../../store/action/dishesActions";
import axiosApi from "../../axiosApi";

const DishForm = ({history, match}) => {
    const {loading} = useSelector(state => state.dishes);
    const dispatch = useDispatch();
    const [dish, setDish] = useState({
        name: '',
        price: "",
        image: ''
    });

    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosApi.get(`/dishes/${match.params.id}.json`);
            setDish(response.data);
        }
        if (match.params.id) {
            fetchData().catch(e => console.error(e));
        }
    }, [match.params.id]);

    const onFormSubmit = async e => {
        e.preventDefault();
        const toSendDish = {
            ...dish,
            price: parseInt(dish.price)
        };
        if (match.params.id) {
            try {
                await dispatch(updateDish(match.params.id, toSendDish));
                history.push('/');
            } catch (e) {
                console.error(e);
            }
        } else {
            try {
                await dispatch(sendDish(toSendDish));
                history.push('/');
            } catch (e) {
                console.error(e);
            }
        }
    };

    const dataChanged = e => {
        const name = e.target.name;
        const value = e.target.value;

        setDish(prevState => ({
            ...prevState,
            [name]: value
        }));
    };


    return (
        <Container>
            <Preloader show={loading} />
            <Backdrop show={loading}/>
            <Form onSubmit={onFormSubmit}>
                <Form.Group className="mb-3" controlId="formName">
                    <Form.Label>Name</Form.Label>
                    <Form.Control onChange={dataChanged} value={dish.name} name="name" type="text" placeholder="Enter dish's name" />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formPrice">
                    <Form.Label>Price</Form.Label>
                    <Form.Control onChange={dataChanged} value={dish.price} name="price" type="number" placeholder="Enter dish's price" />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formImage">
                    <Form.Label>Image</Form.Label>
                    <Form.Control onChange={dataChanged} value={dish.image} name="image" type="text" placeholder="Paste dish's image url" />
                    <Form.Text>
                        Enter url of image
                    </Form.Text>
                </Form.Group>
                <Button variant="primary" type="submit">
                    Create
                </Button>
            </Form>
        </Container>
    );
};

export default DishForm;