import React from 'react';
import {Container, Navbar} from "react-bootstrap";
import NavigationItems from "../../Navigation/NavigationItems";

const Toolbar = () => {
    return (
        <header className="Toolbar">
            <Navbar bg="light" expand="lg">
                <Container>
                    <NavigationItems/>
                </Container>
            </Navbar>
        </header>
    );
};

export default Toolbar;