import React from 'react';
import {Button, Card} from "react-bootstrap";

const DishItem = ({name, price, img, deleted, updatePath}) => {
    return (
        <div className="col-3 mx-3 mb-3">
            <Card>
                <Card.Img variant="top" src={img} />
                <Card.Body>
                    <Card.Title>{name}</Card.Title>
                    <Card.Text>
                        Price: {price} KGS
                    </Card.Text>
                    <Button onClick={deleted} className="mx-1" variant="danger">
                        Delete
                    </Button>
                    <Button href={updatePath} className="mx-1" variant="primary">
                        Update
                    </Button>
                </Card.Body>
            </Card>
        </div>
    );
};

export default DishItem;