import React from 'react';
import {Button, Card} from "react-bootstrap";

const DishCartItem = ({name, price, img, added, removed}) => {
    return (
        <div className="col-3 mx-3 mb-3">
            <Card>
                <Card.Img variant="top" src={img} />
                <Card.Body>
                    <Card.Title>{name}</Card.Title>
                    <Card.Text>
                        Price: {price} KGS
                    </Card.Text>
                    <Button onClick={added} className="mx-1" variant="warning">
                        Add
                    </Button>
                    <Button onClick={removed} className="mx-1" variant="danger">
                        Remove
                    </Button>
                </Card.Body>
            </Card>
        </div>
    );
};

export default DishCartItem;