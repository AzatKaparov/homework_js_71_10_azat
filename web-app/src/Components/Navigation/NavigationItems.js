import React from 'react';
import {Navbar, Nav} from 'react-bootstrap';
import {NavLink} from "react-router-dom";

const NavigationItems = () => {
    return (
        <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
                <NavLink to="/dishes/admin" className="mx-2">Dishes</NavLink>
                <NavLink to="/" className="mx-2">Make order</NavLink>
                <NavLink to="/orders" className="mx-2">Orders</NavLink>
            </Nav>
        </Navbar.Collapse>
    );
};

export default NavigationItems;