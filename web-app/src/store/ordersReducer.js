import {FETCH_ORDERS_SUCCESS} from "./action/ordersActions";

const initialState = {
    orders: [],
};

const ordersReducer = (state=initialState, action) => {
    switch (action.type) {
        case FETCH_ORDERS_SUCCESS:
            return {...state, orders: [...action.orders]};
        default:
            return state;
    }
}

export default ordersReducer;