import axiosApi from "../../axiosApi";

export const FETCH_DISHES_REQUEST = 'FETCH_DISHES_REQUEST';
export const FETCH_DISHES_SUCCESS = 'FETCH_DISHES_SUCCESS';
export const FETCH_DISHES_ERROR = 'FETCH_DISHES_ERROR';
export const SEND_DISH_REQUEST = 'SEND_DISH_REQUEST';
export const SEND_DISH_SUCCESS = 'SEND_DISH_SUCCESS';
export const SEND_DISH_ERROR = 'SEND_DISH_ERROR';
export const DELETE_DISH_REQUEST = 'DELETE_DISH_REQUEST';
export const DELETE_DISH_SUCCESS = 'DELETE_DISH_SUCCESS';
export const DELETE_DISH_ERROR = 'DELETE_DISH_ERROR';

export const fetchDishesRequest = () => ({type: FETCH_DISHES_REQUEST});
export const fetchDishesSuccess = dishes => ({type: FETCH_DISHES_SUCCESS, dishes: dishes});
export const fetchDishesError = error => ({type: FETCH_DISHES_ERROR, error: error});
export const sendDishRequest = () => ({type: SEND_DISH_REQUEST});
export const sendDishSuccess = () => ({type: SEND_DISH_SUCCESS});
export const sendDishError = error => ({type: SEND_DISH_ERROR, error: error});
export const deleteDishRequest = () => ({type: DELETE_DISH_REQUEST});
export const deleteDishSuccess = id => ({type: DELETE_DISH_SUCCESS, id: id});
export const deleteDishError = error => ({type: DELETE_DISH_ERROR, error: error});

export const fetchDishes = () => {
    return async dispatch => {
        dispatch(fetchDishesRequest());

        try {
            const response = await axiosApi.get('./dishes.json');
            dispatch(fetchDishesSuccess(response.data));
        } catch (e) {
            dispatch(fetchDishesError(e))
        }
    }
};

export const sendDish = dish => {
    return async dispatch => {
        dispatch(sendDishRequest);

        try {
            await axiosApi.post(`./dishes.json`, dish);
            dispatch(sendDishSuccess());
        } catch (e) {
            dispatch(sendDishError(e));
        }
    }
}

export const updateDish = (id, dish) => {
    return async dispatch => {
        dispatch(sendDishRequest);

        try {
            await axiosApi.put(`./dishes/${id}.json`, dish);
            dispatch(sendDishSuccess());
        } catch (e) {
            dispatch(sendDishError(e));
        }
    }
}

export const deleteDish = id => {
    return async dispatch => {
        dispatch(deleteDishRequest());

        try {
            await axiosApi.delete(`./dishes/${id}.json`);
            dispatch(deleteDishSuccess(id));
        } catch (e) {
            dispatch(deleteDishError(e));
        }
    }
}