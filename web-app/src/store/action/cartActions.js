export const ADD_TO_CART = "ADD_TO_CART";
export const REMOVE_FROM_CART = "REMOVE_FROM_CART";
export const INIT_CART = "INIT_CART";

export const addToCart = dish => ({type: ADD_TO_CART, dish: dish});
export const removeFromCart = name => ({type: REMOVE_FROM_CART, name: name});
export const initCart = () => ({type: INIT_CART});