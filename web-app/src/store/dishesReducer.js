import {
    DELETE_DISH_ERROR,
    DELETE_DISH_REQUEST,
    DELETE_DISH_SUCCESS,
    FETCH_DISHES_ERROR,
    FETCH_DISHES_REQUEST,
    FETCH_DISHES_SUCCESS,
    SEND_DISH_ERROR,
    SEND_DISH_REQUEST,
    SEND_DISH_SUCCESS
} from "./action/dishesActions";

const initialState = {
    dishes: [],
    loading: null,
    error: null,
};

const parseDishes = obj => {
    return Object.keys(obj).map(key => {
        return {...obj[key], id: key}
    });
};

const dishesReducer = (state=initialState, action) => {
    switch (action.type) {
        case FETCH_DISHES_REQUEST:
            return {...state, loading: true};
        case SEND_DISH_REQUEST:
            return {...state, loading: true};
        case DELETE_DISH_REQUEST:
            return {...state, loading: true};
        case FETCH_DISHES_SUCCESS:
            const parsedData = parseDishes(action.dishes);
            return {...state, loading: false, dishes: parsedData};
        case SEND_DISH_SUCCESS:
            return {...state, loading: false};
        case DELETE_DISH_SUCCESS:
            return {...state, dishes: state.dishes.filter(dish => dish.id !== action.id), loading: false};
        case FETCH_DISHES_ERROR:
            return {...state, loading: false, error: action.error};
        case SEND_DISH_ERROR:
            return {...state, loading: false, error: action.error};
        case SEND_DISH_ERROR || FETCH_DISHES_ERROR:
            return {...state, error: action.error};
        case DELETE_DISH_ERROR:
            return {...state, error: action.error};
        default:
            return state;
    }
}

export default dishesReducer;