import {ADD_TO_CART, INIT_CART, REMOVE_FROM_CART} from "./action/cartActions";

const initialState = {
    cartDishes: [],
};

const cartReducer = (state=initialState, action) => {
    switch (action.type) {
        case INIT_CART:
            return {...state, cartDishes: initialState.cartDishes};
        case ADD_TO_CART:
            let changed = false;
            const existingState = state.cartDishes.map(item => {
                if (item.name === action.dish.name) {
                    changed = true;
                    return {
                        ...item,
                        amount: item.amount + 1
                    };
                }
                return item;
            })
            if (changed) {
                return {...state, cartDishes: existingState};
            } else {
                const newDish = {
                    name: action.dish.name,
                    amount: 1,
                };
                return {...state, cartDishes: [...state.cartDishes, newDish]};
            }
        case REMOVE_FROM_CART:
            return {...state, cartDishes: state.cartDishes.filter(item => item.name !== action.name)};
        default:
            return state;
    }
};

export default cartReducer;