import './App.css';
import DishesContainer from "./Containers/DishesContainer";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import React from "react";
import Layout from "./Components/UI/Layout/Layout";
import DishForm from "./Components/DishForm/DishForm";
import CartContainer from "./Containers/CartContainer";
import OrdersContainer from "./Containers/OrdersContainer";

function App() {
  return (
    <div className="App">
        <BrowserRouter>
          <Layout>
            <Switch>
              <Route path="/" exact component={CartContainer}/>
              <Route path="/dishes/admin" exact component={DishesContainer}/>
              <Route path="/orders" exact component={OrdersContainer}/>
              <Route path="/dishes/add" exact component={DishForm}/>
              <Route path="/dishes/:id/update" exact component={DishForm}/>
              <Route render={() => <h1>Not found</h1>}/>
            </Switch>
          </Layout>
        </BrowserRouter>
    </div>
  );
}

export default App;
