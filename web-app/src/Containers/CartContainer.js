import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {addToCart, initCart, removeFromCart} from "../store/action/cartActions";
import axiosApi from "../axiosApi";
import {fetchDishes} from "../store/action/dishesActions";
import DishCartItem from "../Components/DishItem/DishCartItem";
import {Button} from "react-bootstrap";
import Modal from "../Components/UI/Modal/Modal";

const CartContainer = () => {
    const [showModal, setShowModal] = useState(false);
    const dispatch = useDispatch();
    const {cartDishes} = useSelector(state => state.cart);
    const {dishes} = useSelector(state => state.dishes)
    let totalPrice = 0;
    dishes.forEach(dish => {
        cartDishes.forEach(cartProduct => {
            if (dish.name === cartProduct.name) {
                totalPrice += (cartProduct.amount * dish.price);
            }
        })
    })
    totalPrice += 150;

    const sendOrder = async e => {
        e.preventDefault();
        const order = [...cartDishes];
        try {
            await axiosApi.post('./orders.json', order);
        } finally {
            dispatch(initCart())
        }
    };

    const addProduct = dish => {
        dispatch(addToCart(dish));
    };

    useEffect(() => {
        dispatch(fetchDishes());
    }, [dispatch])

    const dishItems = dishes.map(item => (
        <DishCartItem
            key={item.id}
            name={item.name}
            img={item.image}
            price={item.price}
            added={() => addProduct(item)}
            removed={() => dispatch(removeFromCart(item.name))}
        />
    ));

    return (
        <div className="container">
            <Modal show={showModal} close={() => setShowModal(!showModal)}>
                {cartDishes.map(item => {
                    return dishes.map(dish => {
                        if (item.name === dish.name) {
                            return (
                                <li key={item.id}>{item.name}, {dish.price * item.amount} KGS</li>
                            )
                        }
                    })
                })}
                <li>Delivery: 150</li>
                <li>Total: {totalPrice} KGS</li>
                <div className="row">
                    <Button variant="primary" onClick={sendOrder}>Confirm</Button>
                    <Button className="mx-3" variant="danger" onClick={() => setShowModal(!showModal)}>Cancel</Button>
                </div>
            </Modal>
            {cartDishes.length > 0 &&
                <Button onClick={() => setShowModal(!showModal)} variant="warning">Make order</Button>
            }
            <div className="dishes-block row justify-content-around mt-4">
                {dishItems}
            </div>
        </div>
    );
};

export default CartContainer;