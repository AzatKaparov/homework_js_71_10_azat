import React, {useEffect} from 'react';
import {fetchOrders} from "../store/action/ordersActions";
import {useDispatch, useSelector} from "react-redux";
import {fetchDishes} from "../store/action/dishesActions";

const OrdersContainer = () => {
    const dispatch = useDispatch();
    const {dishes} = useSelector(state => state.dishes);
    const {orders} = useSelector(state => state.orders);

    console.log(orders);

    useEffect(() => {
        dispatch(fetchOrders());
        dispatch(fetchDishes())
    }, []);

    return (
        <div className='container'>
        </div>
    );
};

export default OrdersContainer;