import React, {useEffect} from 'react';
import {Button} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {deleteDish, fetchDishes} from "../store/action/dishesActions";
import DishItem from "../Components/DishItem/DishItem";
import Preloader from "../Components/UI/Preloader/Preloader";
import Backdrop from "../Components/UI/Backdrop/Backdrop";

const DishesContainer = () => {
    const dispatch = useDispatch();
    const {dishes, loading} = useSelector(state => state.dishes)

    const deletePizza = (id) => {
        dispatch(deleteDish(id));
    }

    const dishItems = dishes.map(item => (
        <DishItem
            key={item.id}
            name={item.name}
            img={item.image}
            price={item.price}
            deleted={() => deletePizza(item.id)}
            updatePath={`/dishes/${item.id}/update`}
        />
    ));

    useEffect(() => {
        dispatch(fetchDishes());
    }, [dispatch])

    return (
        <>
            <Preloader show={loading} />
            <Backdrop show={loading}/>
            <div className="container">
                <div className="d-flex justify-content-between align-items-center">
                    <h2>Dishes</h2>
                    <Button variant="success" href="/dishes/add">Add new dish</Button>
                </div>
                <div className="dishes-block row justify-content-around mt-4">
                    {dishItems}
                </div>
            </div>
        </>
    );
};

export default DishesContainer;