import axios from "axios";

const axiosApi = axios.create({
    baseURL: 'https://lab-72-1c54d-default-rtdb.firebaseio.com/'
});

export default axiosApi;