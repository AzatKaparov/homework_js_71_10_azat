import React from 'react';
import ReactDOM from 'react-dom';
import {applyMiddleware, combineReducers, createStore} from 'redux';
import {Provider} from 'react-redux';
import thunk from "redux-thunk";
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import reportWebVitals from './reportWebVitals';
import dishesReducer from "./store/dishesReducer";
import ordersReducer from "./store/ordersReducer";
import App from "./App";
import cartReducer from "./store/cartReducer";


const rootReducer = combineReducers({
    dishes: dishesReducer,
    orders: ordersReducer,
    cart: cartReducer,
})

const store = createStore(
    rootReducer,
    applyMiddleware(thunk)
);

const app = (
    <Provider store={store}>
        <App/>
    </Provider>
)

ReactDOM.render(
    app,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
